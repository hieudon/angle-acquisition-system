delete(instrfindall);

s = serial('COM7');     %choose COM port
set(s, 'Baudrate', 19200);   %sets the Baudrate to the same thing as the Esduino
s.Terminator = 'CR';      %define terminator sent by Esduino
fopen(s);   %open the port

signal = animatedline('Color','k');
time=0;

while(1)
    digital = str2double(fgetl(s));
    %analog = ((digital*3.3/4095)-1.65)/0.3;
    %angle = real(asin(analog)*180/pi);
    addpoints(signal,time,digital); 
    drawnow
    time=time+1;
end

fclose(s);  %close the port
delete(s);  
% clear s;