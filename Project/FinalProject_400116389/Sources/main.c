  #include <hidef.h>
  #include "derivative.h"
  #include "SCI.h" //allows us to use serial communication

  char string[20];  


  /* DECLARE ALL YOUR GLOBAL VARIABLES BELOW*/
    /////////////////////////////////////////////////////
  int mode;  
  unsigned short int val = 0; 
  int bVal = 0;  
  float degree;
   
  void BCD (int bcdVal);
  void BCDBar (int bcdVal);
  void setClk(void);
  void delay1ms(int multiple);

  //---------------------OutCRLF---------------------
  // Output a CR,LF to SCI to move cursor to a new line
  // Input: none
  // Output: none
  // Toggle LED each time through the loop

  void OutCRLF(void){
    SCI_OutChar(CR);
    SCI_OutChar(LF);
  }

  void main(void) {

    /* ESDUINO PORT CONFIGURATION BELOW (Don't Edit) */
    //Set Ports
    DDRJ = 0x01;  	//set all port J as output
    DDR1AD = 0x0F;
    DDRP = 0x1F; 
    
    
    
    /* The next six assignment statements configure the Timer Input Capture */                                                  	
    TSCR1 = 0x90;	//Timer System Control Register 1
                      // TSCR1[7] = TEN:  Timer Enable (0-disable, 1-enable)
                  	// TSCR1[6] = TSWAI:  Timer runs during WAI (0-enable, 1-disable)
                  	// TSCR1[5] = TSFRZ:  Timer runs during WAI (0-enable, 1-disable)
          	        // TSCR1[4] = TFFCA:  Timer Fast Flag Clear All (0-normal 1-read/write clears interrupt flags)
                  	// TSCR1[3] = PRT:  Precision Timer (0-legacy, 1-precision)
                  	// TSCR1[2:0] not used
   
    TSCR2 = 0x04;	//Timer System Control Register 2
                  	// TSCR2[7] = TOI: Timer Overflow Interrupt Enable (0-inhibited, 1-hardware irq when TOF=1)
                  	// TSCR2[6:3] not used
                  	// TSCR2[2:0] = Timer Prescaler Select: See Table22-12 of MC9S12G Family Reference Manual r1.25 (set for bus/1)
   
                  	
    TIOS = 0xFC; 	//Timer Input Capture or Output capture
                  	//set TIC[0] and input (similar to DDR)
    PERT = 0x03; 	//Enable Pull-Up resistor on TIC[0]
   
    TCTL3 = 0x00;	//TCTL3 & TCTL4 configure which edge(s) to capture
    TCTL4 = 0x0A;	//Configured for falling edge on TIC[0]
   

    /* The next one assignment statement configures the Timer Interrupt Enable */        	                                       
    TIE = 0x01;  	//Timer Interrupt Enable
   
    /* The next one assignment statement configures the ESDX to catch Interrupt Requests */           
    IRQCR=0x00;
  	EnableInterrupts;
  	
    /* set the E-Clock speed */
  	setClk(); 
  	
    /* Set Baud Rate */
    SCI_Init(19200);
    
    
    /* Next 4 statements are configured for ADC - see datasheets for more details */
    ATDCTL1 = 0x4F; //configure for 10 bit resolution
    ATDCTL3 = 0x88; //right justified
    ATDCTL4 = 0x00; //prescaler set to 0
    ATDCTL5 = 0x24; //continous conversion on channel 4
   
   
   while(1)                    
      {     
      // if not off
      if(mode!=0){
      
      // sample
      val = ATDDR0;
      
      // calcilate using linear approximations
      if(val>=1352 && val<1670){
      if(mode==1){
      degree = -0.2830*val + 382.6;
      } else{
      degree=-0.3180*val + 430.0; 
      }
      } else if(val<1352 && val>1130){
         degree=-0.4054*val + 548.1;
      }
      
      // make positive if negative
      if(degree<0){
        degree=degree*-1;
      }
           // serial transmit to matlab
           SCI_OutUDec(degree);
           SCI_OutChar(CR);
          
          // pass the right value to BAR function with specific range
           if (mode == 1) {
           bVal = 0; 
           
           PTJ ^= 0x00;
           
           if (degree >0 && degree <= 10) {
            bVal = 1; 
           
          } else if (degree <= 20) {
            bVal = 2;
          }
          else if (degree <= 30) {
             bVal = 3;
          }
          else if (degree <= 40) {
               bVal = 4;
          }
          else if (degree <= 50) {
                 bVal = 5;
          }
          else if (degree <= 60) {
                   bVal = 6;
          }
          else if (degree <= 70) {
            bVal = 7;
          }
          else if (degree <= 80) {
             bVal = 8;
          } else {
             bVal = 9;
          }
          BCDBar (bVal);
          // do BCD if in mode 2
          // pass the degree value calculated to the function
          } else if (mode == 2) {

             BCD(degree); 
          }
        }
       }
      }

  interrupt  VectorNumber_Vtimch0 void ISR_Vtimch0(void)
  {
    unsigned int temp
   
     // go to the following mode 1 if mode 0 ;
    if (mode == 0)
    {
  	  mode = 1;
  	  OutCRLF;
      SCI_OutString("Communication ON");
      SCI_OutString("Bar LED");
    }
     // go to the following mode 2 if mode 1 ;
    else if (mode == 1)
    {  
    	mode = 2;
    	OutCRLF;
   	  SCI_OutString("B C D LED");
    }
     // go to the following mode 0 if mode 2;
    else if (mode == 2)
    {  
    	mode = 0;
    	OutCRLF;
      SCI_OutString("Communication OFF");
   	  
    }
    temp = TC0;
  }

  // clock configuration
  void setClk(void)
  {
   
    CPMUCLKS = 0x80; // choose PLL = 1
    CPMUOSC = 0x00; // source clock chosen
    CPMUPROT = 0x26; // protection of clock config is enable
    // now frequency_ref = 1MHz
    CPMUSYNR = 0x0F; // VCOFRQ = 00, SYNDIN = 15 = F; 32MHz = VCOCLK = 2*1MHz*(1+SYNDIV);
    CPMUFLG = 0x00; // LOCK = 0
    // PLLCLK = 32MHz/4 = 8MHz and BUSCLK = PLLCLK/2 = 4MHz! bus speed set to 4MHz  
    CPMUPROT = 1; //enable clock protection 
  }



  //Delay Function
  void delay1ms(int delayBy)
  {
      int a;
      TSCR1 = 0x90; // Enable time and clear fast timer flag
      TSCR2 = 0x00; // Disable timer interrupt, set prescaler to 1        
      
      TIOS = 0x02;  // input capture for 0-1 and output capture for the rest
      TC1 = TCNT + 4000; // Delay by 1e-4 
      
      for (a = 0; a < delayBy; a++) {
        
        while(!(TFLG1_C1F)); 
        
        TC1 += 4000;
      }
      
  }
    
  void BCD (int bcdVal){
      
      unsigned int ones;
      unsigned int tens;
      unsigned int temp;
      
      temp = bcdVal;
      temp = temp%100;
      // get tens digit
       
      tens = temp/10;
      // get ones digit
      ones = temp%10;

      PT1AD = ones;
      PTP = tens;

      
  }

  void BCDBar (int bcdVal) 
  {
     
     // 0-10 flash 1 LED
     if(bcdVal == 1) 
     {
        PT1AD = 0b00000001;
        PTP = 0b00000000;
     } 
     // 10-20 flash 2 LED
     else if(bcdVal == 2)  
     {
        PT1AD = 0b00000011;
        PTP = 0b00000000;
     }
     // 20-30 flash 3 LED
     else if(bcdVal == 3)  
     {
        PT1AD = 0b00000111;
        PTP = 0b00000000;
     }
     // 30-40 flash 4 LED
     else if(bcdVal == 4)  
     {
        PT1AD = 0b00001111;
        PTP = 0b00000000;
     }
     // 40-50 flash 5 LED
     else if (bcdVal == 5) 
     {
        PT1AD = 0b00001111;
        PTP = 0b00000001;
     }
     // 50-60 flash 6 LED
     else if(bcdVal == 6)  
     {
        PT1AD = 0b00001111;
        PTP = 0b00000011;
     }
     // 60-70 flash 7 LED
     else if(bcdVal == 7)  
     {
        PT1AD = 0b00001111;
        PTP = 0b00000111;
     }
     // 70-80 flash 8 LED
     else if(bcdVal == 8)  
     {
        PT1AD = 0b00001111;
        PTP = 0b00001111;
     }
     // 80-90 flash 9 LED
     else if(bcdVal == 9)  
     {
        PT1AD = 0b00001111;
        PTP = 0b00011111;
     }
     else 
     {
        PT1AD = 0x00;
        PTP = 0x00;
     }
      
  }
    