// filename ******** Main.C ************** 
// Example main program for SCI 9S12C32 gadlfy example   
// 9S12C32 SCI connected to PC com port
// does not run in BOOT mode, only in RUN mode

//  This example accompanies the books
//   "Embedded Microcomputer Systems: Real Time Interfacing", 
//     Thomsen, copyright (c) 2006,
//   "Introduction to Embedded Microcomputer Systems: 
//    Motorola 6811 and 6812 Simulation", Brooks-Cole, copyright (c) 2002,
//    Jonathan W. Valvano 3/17/07

// You may use, edit, run or distribute this file 
//    as long as the above copyright notices remain

//**********************************************************************************
// Adapted for the Technological Arts EsduinoXtreme board by Carl Barnes, 11/28/2014
//**********************************************************************************

#include <hidef.h>      /* common defines and macros */
#include "derivative.h"  /* derivative information */
#include "SCI.h"
#include "SCI.c"


//---------------------OutCRLF---------------------
// Output a CR,LF to SCI to go to a new line
// Input: none
// Output: none
// toggle PJ0 each time through the loop

void OutCRLF(void){
  SCI_OutChar(CR);
  SCI_OutChar(LF);
  PTJ ^= 0x20;          // toggle LED D2
}

char string[20];
unsigned short n;  
void main(void) {		
  DDRJ |= 0x01;     // PortJ bit 0 is output to LED D2 on DIG13
  SCI_Init(9600);
  
  SCI_OutString("Technological Arts - EsduinoXtreme SCI demo"); OutCRLF();
  for(;;) {
    PTJ ^= 0x01;          // toggle LED
    SCI_OutString("Input String: "); 
    SCI_InString(string,19);   
    SCI_OutString("  Output String= "); SCI_OutString(string); OutCRLF();
    
    SCI_OutString("Input Decimal: ");  n=SCI_InUDec(); 
    SCI_OutString("  Output Decimal= "); SCI_OutUDec(n); OutCRLF();
     
    SCI_OutString("Input Hex: ");  n=SCI_InUHex(); 
    SCI_OutString("  Output Hex= "); SCI_OutUHex(n); OutCRLF();
  }

}

